[comment]: # (THEME = solarized)
[comment]: # (CODE_THEME = zenburn)
[comment]: # (The list of themes is at https://revealjs.com/themes/)
[comment]: # (The list of code themes is at https://highlightjs.org/)
[comment]: # (Pass optional settings to reveal.js:)
[comment]: # (controls: true)
[comment]: # (controlsTutorial: true)
[comment]: # (keyboard: true)
[comment]: # (progress: true)
[comment]: # (slideNumber: true)
[comment]: # (showSlideNumber: 'all')
[comment]: # (markdown: { smartypants: true })
[comment]: # (hash: true)
[comment]: # (respondToHashChanges: true)
[comment]: # (Other settings are documented at https://revealjs.com/config/)

CyberPrez #02 - Ansible
# Comprendre Ansible 

[comment]: # (!!! data-background-color="#f6e58d")

Le but de cette présentation est de faire découvrir Ansible aux futurs utilisateurs. Il ne faut pas prendre cela pour un tutoriel dans un cadre professionnel.
<br>
En revanche, n'hésitez pas à partager et m'envoyer vos questions. Le site sera mis à jour si nécéssaire :)

[comment]: # (!!! data-background-color="#f6e58d")

# Qu'est ce que Ansible ? 

[comment]: # (!!! data-background-color="#f6e58d")

Ansible est un **outil d'orchestration**. Il permet de *controler* une ou plusieurs machines par ssh dans le but d'automatiser certaines taches. 

[comment]: # (!!! data-background-color="#f6e58d")

Vous devrez fournir à Ansible un fichier descriptif détaillant comment est-ce que la VM doit être configurée. 
On nomme ce fichier **"un playbook"**.  <br>
Si la machine est déjà correctement configurée, Ansible vous en informera et ne fera rien de plus. 

[comment]: # (!!! data-background-color="#f6e58d")

Les forces de Ansible sont simples: 
- Une communauté très active pour ajouter des fonctionnalités (Profitant de la popularité du Python)
[comment]: # (!!! data-background-color="#f6e58d" data-auto-animate )

Les forces de Ansible sont simples: 
- Une communauté très active pour ajouter des fonctionnalités (Profitant de la popularité du Python)
- Utilisation du YAML facilitant son utilisation 
[comment]: # (!!! data-background-color="#f6e58d" data-auto-animate )

Les forces de Ansible sont simples: 
- Une communauté très active pour ajouter des fonctionnalités (Profitant de la popularité du Python)
- Utilisation du YAML facilitant son utilisation 
- Agentless (Aucun agent à installer sur les machines à piloter, SSH suffit)

[comment]: # (!!! data-background-color="#f6e58d" data-auto-animate )

Voici un playbook simple pour voir ça concretement : 
```yaml
- hosts: all
  tasks:
  - name: "Installer Vim et Emacs (pour pas faire de jaloux)"
    apt:
      name:
        - vim
        - emacs
      state: present
```
*Explication étape-par-étape en dessous.*

[comment]: # (||| data-background-color="#8596BE" data-auto-animate )
```yaml [1-1]
- hosts: all
  tasks:
  - name: "Installer Vim et Emacs (pour pas faire de jaloux)"
    apt:
      name:
        - vim
        - emacs
      state: present
```
Nous prennons toute la liste des hôtes disponibles. 
Cette liste est donnée dans la commande Ansible. 
[comment]: # (||| data-background-color="#8596BE" data-auto-animate )
```yaml [3-8]
- hosts: all
  tasks:
  - name: "Installer Vim et Emacs (pour pas faire de jaloux)"
    apt:
      name:
        - vim
        - emacs
      state: present
```

Nous créons la tache *Installer Vim et Emacs (pour pas faire de jaloux)* qui appele le module **apt** de Ansible. 
On s'assure que les paquets vim et emacs soient présents via le *"state: present"*. 

*On peut également les désinstaller avec l'instruction, state: absent.*
[comment]: # (||| data-background-color="#8596BE" data-auto-animate )

Maintenant que nous avons le playbook. Il faut générer notre **inventaire** !
Celui-ci doit contenir les IPs (*et quelques petites informations*).

[comment]: # (!!! data-background-color="#f1c40f" data-auto-animate )
Voici à quoi peut ressembler un inventaire: 
```ini
[master]
192.168.122.10 ansible_user=admin
192.168.122.11 ansible_user=master

[worker]
192.168.122.12 ansible_port=2222
192.168.122.13
```
[comment]: # (!!! data-background-color="#f1c40f" data-auto-animate )

Cet inventaire représente 2 groupes de machine: **master** et **worker** qui contiennent chacun 2 machines. 
Si mon playbook mentionne : **hosts: all**, les 2 groupes seront utilisés. Sinon, je peux choisir à quel groupe s'adresse mes taches avec **hosts: nom-du-groupe**. 

J'ai également placé quelques paramètres spécifiques à des machines *(comme par exemple l'utilisateur sur lequel se connecter, ou le port ssh)*. 

